---
title: Repository
layout: page
---


## Packages
- OE openpli 3.0, openpli 4.0, opendreambox 2.0 (python2.7) <br>
  для dreambox, vu и других [mips32el](mips32el/) <br>
  **только** для dm800 [mips32el-nf](mips32el-nf/) <br>
- OE openpli 2.x, opendreambox 1.6 (python2.6)
  [mipsel](mipsel/)
- SH4 amiko alien и другие
  [sh4](sh4/)
- OE opendreambox 2.2 (dm820hd dm7080hd)
  [OE 2.2](mipsel22/)

## Picons
Скачать пиконы можно по [этой ссылке](picon/).
