---
title: Developer guide
layout: page
---


Инструкции для разработчиков
============================
<p class="toc"></p> 

## Cоздание скинов
Как сделать или адаптировать уже имеющийся скин для плагина.
Инструкция по созданию своих скинов для плагина

### Редактирование с помощью e2skinner
К сожалению [e2skinner](http://code.google.com/p/e2skinner2/) рассчитан на редактирование полного скина дримбокса, а не скина отдельного плагина. Поэтому прийдёться подготовить наш скин для e2skinner.

* Сначала скачиваем [e2skinner](http://code.google.com/p/e2skinner2/downloads/list), распаковываем, так же берём *skin-post* и *skin-pre* [отсюда](http://code.google.com/p/kartinatv-dm/downloads/list).

* Скачиваем с дримбокса папку /usr/share/enigma2/KartinaTV_skin и кладём в папку skins в каталоге e2skinner2.0.6.3b (версия на момент написания wiki). В эту же папку кладём _skin-post.exe_ и _skin-pre.exe_.

* открываем `kartina_skin.xml` _skin-pre.exe_ (для этого можно просто перетащить файл  `kartina_skin.xml` на _skin-pre.exe_). В каталоге создался файл `skin.xml`
 
* открываем программу e2skinner и начинаем редактировать скин. В этой программе удобно менять расположение элементов. Когда достигли нужного вида не забываем нажать кнопку сохранить.
 
* теперь надо запустить утилиту _skin-post.exe_. Точно так же нужно открыть `skin.xml` </font> в _skin-post.exe_. Создастся обновлённый файл `kartina_skin.xml`, который можно обратно кидать в дримбокс!
 
* На всякий случай перед перезаписью `skin.xml` и `kartina_skin.xml` утилиты _skin-pre.exe_ и _skin-post.exe_ создают бэкапы `skin.xml.backup` и `kartina_skin.xml.backup`

### Элементы скина

_Названия элементов в разработке. Внимание возможны изменения!!!_


#### Элементы инфобара

|Name|Описание|Тип элемента|
|----|--------|------------|
|channelName|Имя просматриваемого канала|Label()|
|currentName|Краткое описание текущей передачи|Label()|
|nextName|Краткое описание следующей передачи|Label()|
|currentTime|Время начала текущей передачи в формате HH:MM|Label()|
|nextTime|Время начала следующей передачи в формате HH:MM|Label()|
|currentDuration|Время которое осталось до окончания текущей передачи в формате +%d min|Label()|
|nextDuration|Длительность следующей передачи в формате %d min|Label()|
|progressBar|Отображает отношение времени которое уже прошло ко всей длине передачи|Slider()|
|archiveDate|Дата передачи из архива в формате dd.mm|Label()|
|!KartinaInArchive|Отображает включен режим архива или лайв режим|source Boolean()|
|playPause|пока не определено|?|

#### Элементы списка каналов

|Name|Описание|Тип элемента|
|----|--------|------------|
|channelName|Имя просматриваемого канала|Label()|
|epgName|Краткое описание текущей передачи|Label()|
|epgNextName|Кратикое описание следующей передачи|Label()|
|epgTime|Время начала и окончания текущей передачи в формате HH:MM - HH:MM|Label()|
|epgNextTime|Время начала и окончания следующей передачи в формате HH:MM - HH:MM|Label()|
|epgDescription|Подробное описание текущей передачи |Label()|
|epgNextDescription|Подробное описание следующей передачи|Label()|
|epgProgress|Отображает отношение времени которое уже прошло ко всей длине передачи|Slider()|


#### Элементы ЕПГ меню

|Name|Описание|Тип элемента|
|----|--------|------------|
|epgName|Краткое описание выбранной передачи|Label()|
|epgDescription|Подробное описание выбранной передачи|Label()|
|epgTime|Время начала и окончания выбранной передачи в формате HH:MM - HH:MM|Label()|
|sepgName|Для single режима. Краткое описание|Label()|
|sepgDescription|Для single режима. подробное онисание |Label()|
|sepgTime|Время начала и окончания выбранной передачи в формате HH:MM |Label()|

### Примеры
Это реально, например *maloj19* подогнал [такой скин!](http://www.allrussian.info/index.php?page=Thread&postID=2079253#post2079253)

## ID для пиконов
обновлялсь давно скорее всего устарели

### KartinaTV
|id|название|
|--|--------|
|2|Первый|
|3|Россия 1|
|4|НТВ|
|5|ТВЦ|
|7|РенТВ|
|8|ТНТ|
|9|СТС|
|14|Домашний|
|15|ДТВ|
|61|5 канал Петербург|
|139|World Fashion|
|13|Ностальгия|
|24|Охота и рыбалка|
|26|Россия К|
|28|24 док|
|41|Совершенно секретно|
|51|Кухня ТВ|
|59|Зоопарк|
|71|Discovery|
|89|Discovery Science|
|205|Russian Travel Guide|
|233|Animal Planet|
|235|National Geographic Channel|
|237|Discovery World|
|253|ТелеКафе|
|6|Россия 24|
|73|РБК ТВ|
|339|EuroNews|
|23|Бульвар ТВ|
|35|2x2|
|43|Первый игровой|
|57|Интересное ТВ|
|77|Парк развлечений|
|85|ТВ3|
|91|Время|
|343|ЮморТВ|
|18|Теленяня|
|19|Бибигон|
|20|Мультимания|
|65|Знание|
|239|Disney Channel|
|241|Jim Jam|
|243|Nickelodeon|
|21|Музыка Первого|
|22|RU TV|
|37|МузТВ|
|39|МТV Россия|
|49|Ля Минор|
|181|Первый молодёжный|
|227|OTV|
|229|Music Box RU|
|231|Music Box TV|
|291|Первый музыкальный|
|369|RuSong|
|11|Комеди ТВ|
|16|НСТ|
|25|Россия 2|
|27|Боец|
|31|7-ТВ|
|45|Русский экстрим|
|53|Наш хоккей|
|63|Спорт 2|
|157|Футбол 1|
|177|Футбол 2|
|209|Спорт 1|
|245|Спортивное ТВ|
|247|Теннис|
|29|1+1|
|30|5 канал|
|33|Inter+|
|47|ТСВ|
|67|СТБ|
|69|ICTV|
|81|2+2|
|83|Enter-фильм|
|129|CNL|
|141|Lider TV|
|143|Shant TV|
|145|Новый канал|
|147|Казахстан - CaspioNet|
|149|Беларусь ТВ|
|151|Грозный ТВ|
|153|ТРК Украина|
|213|Израиль+|
|269|ТБН Россия|
|293|Impact|
|365|Брат ТВ|
|367|Библейский маяк|
|10|МНОГОсерийное ТВ|
|12|Дом кино|
|17|Индия ТВ|
|75|TV1000|
|79|Иллюзион +|
|87|Звезда|
|163|Кинозал 1|
|165|Кинозал 2|
|167|Кинозал 3|
|169|Кинозал 4|
|171|Кинозал 5|
|173|Кинозал 6|
|249|TV1000 Русское Кино|
|251|Diva Universal|
|255|Феникс-Кино|
|259|Кино-Non Stop|
|261|Кинозал HD 1|
|295|Кинозал HD 2|
|297|Кинозал HD 3|
|309|Кинозал HD 4|
|155|XXX1|
|159|XXX2|
|161|XXX3|
|257|Русская ночь|
|93|Музыка Авторадио|
|95|Love Radio|
|99|Шансон|
|101|Ретро FM|
|107|Эхо Москвы|
|111|Наше Радио|
|113|Ultra|
|131|Дети FM|
|183|LIVETIME Radio|
|189|Радио Дача|
|191|Юмор ФМ|
|201|Релакс ФМ|
|203|Радио Давидсон|
|211|Радио Германия|
|217|NVC Радио|


### РодноеТВ
|id|название|
|--|--------|
|12|DISCOVERY|
|13|ДОМ КИНО|
|14|TV XXI|
|15|ИНТЕР|
|17|M2|
|18|MTV Ukr|
|19|СТБ|
|20|M1|
|21|ICTV|
|22|TV-1000 Action|
|23|RU-MUSIC|
|24|ОХОТА и РЫБАЛКА|
|25|TV-1000 Русское Кино|
|26|РУССКИЙ ИЛЛЮЗИОН|
|27|UNIVERSAL|
|28|demo РЕТРО|
|29|НОСТАЛЬГИЯ|
|30|OTV Muzic|
|31|EXTREME SPORTS|
|32|demo 1+1|
|33|DISCOVERY Science|
|34|DISCOVERY World|
|35|ANIMAL PLANET|
|36|VIASAT EXPLORER|
|37|КОМЕДИЯ-TV|
|38|VIASAT HISTORY|
|39|TV-1000|
|42|REN TV|
|43|Н С Т страшное|
|44|DIVA Universal|
|45|Н С Т смешное|
|47|EUROSPORT|
|48|VIASAT SPORT|
|50|EUROSPORT 2|
|51|КАРУСЕЛЬ|
|53|RBK|
|54|RTVI|
|55|1-й АВТОМОБИЛЬНЫЙ|
|58|ДЕТСКИЙ|
|59|demo RBK|
|60|demo RTVI|
|61|РЕТРО|
|62|1+1|
|63|demo ИНТЕР|
|77|EURONEWS|
|78|ДОЖДЬ ТВ|
|81|VIASAT NATURE|
|82|NICKELODEON|
|83|24 ТЕХНО|
|85|ТНТ|
|86|ПАРК РАЗВЛЕЧЕНИЙ|
|87|COMEDY TV|
|88|TRAVEL|
|89|VH1|
|90|ФУТБОЛ|
|91|MGM|
|92|Mezzo|
|93|НАШ ФУТБОЛ|
|94|A-One|
|95|НАШЕ КИНО|
|96|Детский Мир/Телеклуб|
|97|СОВЕРШЕННО СЕКРЕТНО|
|98|Biz-TV|
|99|MUZIC BOX UA|
|100|National Geographic|
|101|СПОРТ 1 укр|
|102|РТР-Планета|
|103|ENTER FILM|
|104|ENTER MUZIC|
|105|НОВЫЙ|
|106|5 канал Украина|
|107|КХЛ|
|108|ФУТБОЛ +|
|109|DA VINCI LEARNING|
|110|BABY TV|
|111|ESPN Classic|
|112|ТРК Украина|
|113|TВ центр|
|114|LTV 1 Latvija|
|118|TV 3 Lietuva|
|119|TV 6 Lietuva|
|120|DISCOVERY investigation|
|121|ИЗРАИЛЬ+|
|122|NAT GEO WILD|
|123|Русский EXTREME|
|125|DISCOVERY Travel and Living|
|126|Viasat Sport Baltic LV|
|127|TV 3 Latvija|
|128|LTV 7 Latvija|
|129|LNT Latvija|
|131|TEST Кинозал 3D|
|132|LV Hockey|
|133|Lietuvos Rytas TV|
|134|LT LNK|
|135|Baltijos TV|
|136|LTV|
|137|TV 6 Latvija|
|138|Viasat Sport Baltic LT|
|139|Кинозал Мульт|
|140|3+ Baltic|
|141|TV 8 Lietuva|
|142|TV 1 Lietuva|
|143|Sport 1 Lietuva|
|144|CARTOON Network / TCM|
|145|ТВТ1|
|146|САФИНА|
|147|BAKHORISTON|
|148|JAHONNAMO|
|149|NICKELODEON Junior / VH-1|
|154|Кинозал (зарубежные фильмы)|
|155|XXX 1|
|156|XXX 2|
|157|XXX 3|
|160|ТЕСТ Кинозал ТЕСТ|
|161|KINO LV|
|162|Кинозал (русские фильмы)|
|164|KINO LT|
|166|7TB|
|168|SHANT TV|
|170|BBC World News|
|171|BBC Entertainment|
|172|К1|
|173|К2|
|174|2+2|
|175|+ФУТБОЛ|
|176|1 BALTIC|
|177|REN TV BALTIC|
|178|ARMENIA1|
|179|+СПОРТ|
|180|KINO ENG|
|181|KINO LT 2|
|184|+ПРЕМЬЕРА|
|185|ШАНСОН ТВ|
|186|Mūzikas Video|
|187|ESPN ameriсa|
|188|БОЕЦ|
|189|Fashion TV|
|195|ДРАЙВ|
|205|LTV 2|
|206|INFO TV|
|207|Balticum TV|
|209|LIUKS|
|210|1 BALTIC MUSIC|
|211|BOOMERANG|
|212|JIM-JAM|
|219|ПЕРВЫЙ|
|220|РОССИЯ|
|221|НТВ|
|222|CTC|
|224|5 КАНАЛ|
|225|МОЯ ПЛАНЕТА|
|226|ПЕРЕЦ|
|227|РОСCИЯ-24|
|228|МУЗЫКА|
|229|РОССИЯ-2|
|230|MTV rus|
|231|ФЕНИКС|
|232|AXN Sci-Fi|
|233|SET|
|234|FOX LIFE|
|235|FOX CRIME|
|236|НАУКА 2.0|
|237|ДИСНЕЙ|
|238|ВРЕМЯ|
|239|КУЛЬТУРА|
|240|ЖИВИ|
|241|МИР|
|242|МУЛЬТИМАНИЯ|
|243|СПОРТ 1|
|244|СПОРТ 2 рус|
|245|ТЕЛЕКАФЕ|
|248|ВОПРОСЫ и ОТВЕТЫ|
|249|ДОМАШНИЙ|
|250|МУЗ ТВ|
|251|ТНТ вл|
|252|ДОЖДЬ ТВ вл|
|253|СПОРТ+ вл|
|254|ФУТБОЛ+ вл|
|255|ПРЕМЬЕРА+ вл|
|256|TEST CHANNEL|


## About site and developer
If you wanna contact me, please write email to _alexeytech at gmail dot com_

Don't hesitate to contact if you have propositions about plugin, site or forum. You can also contact me, report bug or feature on the forum.

These pages are created using [panadoc](http://johnmacfarlane.net/pandoc/) and this [Makefile](Makefile). Also I have added a bit css styling and very little js scripting and finally hosted it on google drive. If you want to see features go to [example page](example.html).
