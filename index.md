---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

![ ](dreambox.png)

# Welcome to IPtvDream homepage!
_Enigma2 IPTV plugin by technic_

### Концепция проекта
IPtvDream плагин создан для просмотра различный iptv сервисов на ресиверах 
Dreambox и других на основе enigma2/Linux. Основные цели проекта сделать просмотр iptv таким  же удобным как спутниковое тв, и дать возможность сторонним разработчикам как можно проще добавлять свои iptv сервисы.

<a class="btn" href="manual.html#установка">Install now</a>

### Screenshot
_картинки увеличиваются_

[<img src="ximages/thumbnail/channel_list.jpg">](images/channel_list.jpg)
[<img src="ximages/thumbnail/infobar.jpg">](images/infobar.jpg)
[<img src="ximages/thumbnail/groups.jpg">](images/groups.jpgg)
[<img src="ximages/thumbnail/epg.jpg">](images/epg.jpg)

_Так выглядит видеотека..._

[<img src="images/thumbnail/video_infobar.jpg">](images/video_infobar.jpg)
[<img src="images/thumbnail/video_list.jpg">](images/video_list.jpg)
[<img src="images/thumbnail/film_search.jpg">](images/film_search.jpg)
[<img src="images/thumbnail/video_genres.jpg">](images/video_genres.jpg)

### Этот плагин вам позволит:
  * Смотреть iptv так же удобно как и обычные спутниковые каналы, так же можно запускать одновременно другие плагины и задачи.
  * Доступен телегид и архив передач с сервера провайдера.
  * Удобный вид epg при выборе каналов, аналогичный спутниковому списку.
  * Возможность легко адаптировать скин плагина под скин вашего имиджа ([Skinning# инструкция]). По умолчанию в плагине установлен отличный скин HydroHD от Ku4a.
  * Быстрый переход по истории переключения каналов кнопками < , >
  * Поддержка избранных каналов с возможностью их _сортировки_, для удобного просмотра.
  * Возможность установить пиконы в инфобаре. Расположение пиконов автоматически  берётся из имиджа, что даёт возможно расположить их в той папке, которую считаете наиболее оптимальной.
  * Информация об окончании абонемента поможет вам вовремя продлить подписку.
  * Опция родительского контроля заблокирует все ххх каналы. Кроме того есть возможность защитить любой другой канал.
  * Возможность переключать аудио-дорожки и просматривать информацию о видео потоке.

Для более подробной информации обязательно прочитайте [**инструкцию по установке и использованию**](manual.html)

<!---
## Поддерживаемые сервисы

| *КартинаТВ* | *РодноеТВ* | *mixiptv* | *Bigfilm* |
|--------|--------|--------| -------- |
| ![](images/logo-kartina.png) | ![](images/logo-rodnoe.png) | ![](images/logo-mixtv.png) | ![](images/logo-bigfilm.png) |
-->


## Проверенные приставки
Dreambox dm800, dm8000, dm500HD, dm800se,
Vu+ SOLO/DUO

На SH4 боксах работает (например amiko-alien). Ведётся тестирование

<!--
## Внешние ресурсы
[Русскоязычный форум](http://www.allrussian.info/index.php?page=Thread&threadID=123462),
[Форум GiClub](https://giclub.tv/index.php?topic=11928),
[репозиторий bitbucket](https://bitbucket.org/account/user/iptvdream/projects/IPTV4X),
[Форум mixtv](http://forum.mixip.tv/viewforum.php?f=8),
[Форум bigfilmtv](https://forum.bigfilm.tv/showthread.php?20-%D0%9F%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD-%D0%B4%D0%BB%D1%8F-enigma2)
-->

## Видео версии 2.0.4
на DM800HD
<iframe width="854" height="510" src="https://www.youtube.com/embed/PvnYHXpo0vw" frameborder="0" allowfullscreen></iframe>

### Дополнительная информация
Это продолжение плагина [картина тв](https://code.google.com/p/kartinatv-dm/)

##### Важная информация
<small>
Здесь предоставлены файлы расширенного мультимедиа плеера, который был разработан сообществом энтузиастов на сайте https://github.com. Авторы ipk и deb пакетов не несут никакой ответственности за контент предоставляемый различными мультимедиа провайдерами.
Пользователь обязан сам удостоверится что выбранный им мультимедиа провайдер соблюдает закон об авторском праве и все другие законы в стране проживания пользователя.
</small>
